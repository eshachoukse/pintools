/*BEGIN_LEGAL 
Intel Open Source License 

Copyright (c) 2002-2015 Intel Corporation. All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */
//
// @ORIGINAL_AUTHOR: Artur Klauser
// @EXTENDED: Rodric Rabbah (rodric@gmail.com) 
//

/*! @file
 *  This file contains an ISA-portable cache simulator
 *  data cache hierarchies
 */


#include "pin.H"

#include <iostream>
#include <fstream>

#include "armpintool.H"
#include "pin_profile.H"

std::ofstream outFile;

/* ===================================================================== */
<<<<<<< HEAD
/* Global Variables */
/* ===================================================================== */

// wrap configuation constants into their own name space to avoid name clashes
const UINT32 max_sets = 8*KILO; // cacheSize / (lineSize * associativity);
const UINT32 max_associativity = 256; // associativity;
const CACHE_ALLOC::STORE_ALLOCATION allocation = CACHE_ALLOC::STORE_ALLOCATE;


CACHE<CACHE_SET::ROUND_ROBIN<max_associativity>, max_sets, allocation> *dl1 = NULL; 
/* ===================================================================== */
=======
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
/* Commandline Switches */
/* ===================================================================== */

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,    "pintool",
    "o", "dcache.out", "specify dcache file name");
KNOB<BOOL>   KnobTrackLoads(KNOB_MODE_WRITEONCE,    "pintool",
    "tl", "0", "track individual loads -- increases profiling time");
KNOB<BOOL>   KnobTrackStores(KNOB_MODE_WRITEONCE,   "pintool",
   "ts", "0", "track individual stores -- increases profiling time");
KNOB<UINT32> KnobThresholdHit(KNOB_MODE_WRITEONCE , "pintool",
   "rh", "100", "only report memops with hit count above threshold");
KNOB<UINT32> KnobThresholdMiss(KNOB_MODE_WRITEONCE, "pintool",
   "rm","100", "only report memops with miss count above threshold");
KNOB<UINT32> KnobCacheSize(KNOB_MODE_WRITEONCE, "pintool",
    "c","2048", "cache size in kilobytes");
KNOB<UINT32> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool",
    "b","64", "cache block size in bytes");
KNOB<UINT32> KnobAssociativity(KNOB_MODE_WRITEONCE, "pintool",
    "a","4", "cache associativity (1 for direct mapped)");
KNOB<BOOL> KnobTraceGen(KNOB_MODE_WRITEONCE, "pintool",
    "ta","0", "trace generation of only addresses of WBs to the memory");
KNOB<BOOL> KnobTraceDataGen(KNOB_MODE_WRITEONCE, "pintool",
    "td","0", "trace generation of data from WBs to the memory");
KNOB<BOOL> KnobCompress(KNOB_MODE_WRITEONCE, "pintool",
    "comp","0", "compress the write back data and report size changes");
KNOB<BOOL> KnobPrintUniqueWrites(KNOB_MODE_WRITEONCE, "pintool",
    "pmw","0", "Print the unique address written back to and the number of times");
<<<<<<< HEAD
KNOB<BOOL> KnobPrintOverTimeWrites(KNOB_MODE_WRITEONCE, "pintool",
    "pwt","0", "Print the number of writes per 100000 instructions");
KNOB<UINT32> KnobDRAMCacheType(KNOB_MODE_WRITEONCE, "pintool",
    "dsc","0", "Scheme to maintain the DRAM cache/scratchpad\n"
	"0 : No DRAM\n"
	"1 : DRAM as cache\n"
	"2 : DRAM as cache, allocate on writes only\n"
	"3 : DRAM as scratchpad, allocate only the pointed addresses\n");
=======
KNOB<BOOL> KnobDRAMcacheON(KNOB_MODE_WRITEONCE, "pintool",
    "drc","0", "Model the DRAM cache/scratchpad");
KNOB<UINT32> KnobDRAMcachetype(KNOB_MODE_WRITEONCE, "pintool",
    "dsc","0", "Scheme to maintain the DRAM cache/scratchpad");
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f

PIN_LOCK Lock;
UINT64 TotalCount = 0;
UINT64 TotalOverflows=0;
REG ScratchReg;

// First parameter is the number of instructions in this basic block.
// Second parameter is the current dynamic instruction count
// Return the new count
//
ADDRINT PIN_FAST_ANALYSIS_CALL DoCount(ADDRINT numInsts, ADDRINT count)
{
<<<<<<< HEAD
	if(KnobPrintOverTimeWrites && (count%100000 == 0))
		outFile << dl1->PrintCurrWrites();
    if((count>>31) & 1) {
		TotalOverflows++;
		return 0;
=======
    if((count>>31) & 1) {
	TotalOverflows++;
	return 0;
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
    }
    return count + numInsts;
}

VOID ThreadStart(THREADID tid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
    // When the thread starts, zero the virtual register that holds the
    // dynamic instruction count.
    //
    PIN_SetContextReg(ctxt, ScratchReg, 0);
}

VOID ThreadFini(THREADID tid, const CONTEXT *ctxt, INT32 code, VOID *v)
{
    // When the thread exits, accumulate the thread's dynamic instruction
    // count into the total.
    PIN_GetLock(&Lock, tid+1);
    TotalCount += PIN_GetContextReg(ctxt, ScratchReg);
    PIN_ReleaseLock(&Lock);
}

VOID Trace(TRACE trace, VOID *v)
{
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
        // The virtual register ScratchReg holds the dynamic instruction
        // count for each thread.  DoCount returns the sum of the basic
        // block instruction count and G0, we write the result back to G0
        BBL_InsertCall(bbl, IPOINT_ANYWHERE, AFUNPTR(DoCount),
                       IARG_FAST_ANALYSIS_CALL,
                       IARG_ADDRINT, BBL_NumIns(bbl),
                       IARG_REG_VALUE, ScratchReg,
                       IARG_RETURN_REGS, ScratchReg,
                       IARG_END);
    }
}
/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    cerr <<
        "This tool represents a cache simulator.\n"
        "\n";

    cerr << KNOB_BASE::StringKnobSummary() << endl; 
    return -1;
}

<<<<<<< HEAD
=======
/* ===================================================================== */
/* Global Variables */
/* ===================================================================== */

// wrap configuation constants into their own name space to avoid name clashes
const UINT32 max_sets = 8*KILO; // cacheSize / (lineSize * associativity);
const UINT32 max_associativity = 256; // associativity;
const CACHE_ALLOC::STORE_ALLOCATION allocation = CACHE_ALLOC::STORE_ALLOCATE;


CACHE<CACHE_SET::ROUND_ROBIN<max_associativity>, max_sets, allocation> *dl1 = NULL; 
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f

typedef enum
{
    COUNTER_MISS = 0,
    COUNTER_HIT = 1,
    COUNTER_NUM
} COUNTER;



typedef  COUNTER_ARRAY<UINT64, COUNTER_NUM> COUNTER_HIT_MISS;


// holds the counters with misses and hits
// conceptually this is an array indexed by instruction address
COMPRESSOR_COUNTER<ADDRINT, UINT32, COUNTER_HIT_MISS> profile;

/* ===================================================================== */

VOID LoadMulti(ADDRINT addr, UINT32 size, UINT32 instId)
{
    // first level D-cache
<<<<<<< HEAD
    const BOOL dl1Hit = dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);
=======
    const BOOL dl1Hit = dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f

    const COUNTER counter = dl1Hit ? COUNTER_HIT : COUNTER_MISS;
    profile[instId][counter]++;
}

/* ===================================================================== */

VOID StoreMulti(ADDRINT addr, UINT32 size, UINT32 instId)
{
    // first level D-cache
<<<<<<< HEAD
    const BOOL dl1Hit = dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);
=======
    const BOOL dl1Hit = dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f

    const COUNTER counter = dl1Hit ? COUNTER_HIT : COUNTER_MISS;
    profile[instId][counter]++;
}

/* ===================================================================== */

VOID LoadSingle(ADDRINT addr, UINT32 instId)
{
    // @todo we may access several cache lines for 
    // first level D-cache
<<<<<<< HEAD
    const BOOL dl1Hit = dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);
=======
    const BOOL dl1Hit = dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f

    const COUNTER counter = dl1Hit ? COUNTER_HIT : COUNTER_MISS;
    profile[instId][counter]++;
}
/* ===================================================================== */

VOID StoreSingle(ADDRINT addr, UINT32 instId, UINT32 OpCount)
{
    // @todo we may access several cache lines for 
    // first level D-cache
<<<<<<< HEAD
    const BOOL dl1Hit = dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);
=======
    const BOOL dl1Hit = dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
    //printf("Opcount : %d\n", OpCount);

    const COUNTER counter = dl1Hit ? COUNTER_HIT : COUNTER_MISS;
    profile[instId][counter]++;
}

/* ===================================================================== */

VOID LoadMultiFast(ADDRINT addr, UINT32 size)
{
<<<<<<< HEAD
    dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);
=======
    dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
}

/* ===================================================================== */

VOID StoreMultiFast(ADDRINT addr, UINT32 size, UINT32 OpCount)
{
<<<<<<< HEAD
    dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);
=======
    dl1->Access(addr, size, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
    //printf("Opcount : %d\n", OpCount);
    
}

/* ===================================================================== */

VOID LoadSingleFast(ADDRINT addr)
{
<<<<<<< HEAD
    dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);    
=======
    dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);    
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
}

/* ===================================================================== */

VOID StoreSingleFast(ADDRINT addr, UINT32 OpCount)
{
<<<<<<< HEAD
    dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType , outFile);    
=======
    dl1->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_STORE, KnobTraceGen, KnobTraceDataGen, KnobCompress, outFile);    
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
    //printf("Opcount : %d\n", OpCount);
}



/* ===================================================================== */

VOID Instruction(INS ins, void * v)
{
    if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
    {
        // map sparse INS addresses to dense IDs
        const ADDRINT iaddr = INS_Address(ins);
        const UINT32 instId = profile.Map(iaddr);

        const UINT32 size = INS_MemoryReadSize(ins);
        const BOOL   single = (size <= 4);
                
        if( KnobTrackLoads )
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE, (AFUNPTR) LoadSingle,
                    IARG_MEMORYREAD_EA,
                    IARG_UINT32, instId,
                    IARG_END);
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) LoadMulti,
                    IARG_MEMORYREAD_EA,
                    IARG_MEMORYREAD_SIZE,
                    IARG_UINT32, instId,
                    IARG_END);
            }
                
        }
        else
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) LoadSingleFast,
                    IARG_MEMORYREAD_EA,
                    IARG_END);
                        
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) LoadMultiFast,
                    IARG_MEMORYREAD_EA,
                    IARG_MEMORYREAD_SIZE,
                    IARG_END);
            }
        }
    }
        
    if ( INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
    {
        // map sparse INS addresses to dense IDs
        const ADDRINT iaddr = INS_Address(ins);
        const UINT32 instId = profile.Map(iaddr);
            
        const UINT32 size = INS_MemoryWriteSize(ins);

        const BOOL   single = (size <= 4);
                
        if( KnobTrackStores )
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreSingle,
                    IARG_MEMORYWRITE_EA,
                    IARG_UINT32, instId,
		    INS_OperandCount(ins),
                    IARG_END);
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreMulti,
                    IARG_MEMORYWRITE_EA,
                    IARG_MEMORYWRITE_SIZE,
                    IARG_UINT32, instId,
                    IARG_END);
            }
                
        }
        else
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreSingleFast,
                    IARG_MEMORYWRITE_EA,
		    IARG_UINT32, INS_OperandCount(ins),
                    IARG_END);
                        
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreMultiFast,
                    IARG_MEMORYWRITE_EA,
                    IARG_MEMORYWRITE_SIZE,
		    IARG_UINT32, INS_OperandCount(ins),
                    IARG_END);
            }
        }
            
    }
}

/* ===================================================================== */

VOID Fini(int code, VOID * v)
{
    // print D-cache profile
    // @todo what does this print
    
    //outFile << dl1->PrintCompression();
    outFile << "PIN:MEMLATENCIES 1.0. 0x0\n";
            
    outFile <<
        "#\n"
        "# DCACHE stats\n"
        "#\n";
<<<<<<< HEAD
    dl1->FlushAll(KnobTraceGen, KnobTraceDataGen, KnobCompress, KnobDRAMCacheType, outFile); 
	if(KnobPrintUniqueWrites)
		dl1->PrintMemUnique(outFile);
    outFile << dl1->StatsLong("# ", CACHE_BASE::CACHE_TYPE_DCACHE, KnobCompress);
	if(KnobDRAMCacheType)
		outFile << "\n#DRAM STATS\n" <<  dl1->PrintDRAMStats();
=======
    dl1->FlushAll(); 
    if(KnobPrintUniqueWrites)
	dl1->PrintMemUnique(outFile);
    outFile << dl1->StatsLong("# ", CACHE_BASE::CACHE_TYPE_DCACHE, KnobCompress);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f

    if( KnobTrackLoads || KnobTrackStores ) {
        outFile <<
            "#\n"
            "# LOAD stats\n"
            "#\n";
        
        outFile << profile.StringLong();
    }

    //Taking care of the overflow
    UINT64 ThirtyTwo = 1;
    UINT64 OverflowAdd = (ThirtyTwo<<32)*TotalOverflows;
    TotalCount = OverflowAdd + TotalCount;
    outFile << "Instruction Count= " << TotalCount << " Overflow= " << TotalOverflows<< endl;
	
    outFile.close();
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
    PIN_InitSymbols();

    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }

    cout << "In main : TraceGen : " << KnobTraceGen << "\n";
    outFile.open(KnobOutputFile.Value().c_str());

    dl1 = new CACHE<CACHE_SET::ROUND_ROBIN<max_associativity>, max_sets, allocation>("L1 Data Cache", 
                         KnobCacheSize.Value() * KILO,
                         KnobLineSize.Value(),
<<<<<<< HEAD
                         KnobAssociativity.Value(), KnobDRAMCacheType.Value(), outFile);
=======
                         KnobAssociativity.Value(), outFile);
>>>>>>> 871073d85c487c85185a8108e71af461965cd66f
    
    profile.SetKeyName("iaddr          ");
    profile.SetCounterName("dcache:miss        dcache:hit");

    COUNTER_HIT_MISS threshold;
    PIN_InitLock(&Lock);

    ScratchReg = PIN_ClaimToolRegister();
    if (!REG_valid(ScratchReg))
    {
        std::cerr << "Cannot allocate a scratch register.\n";
        std::cerr << std::flush;
        return 1;
    }


    threshold[COUNTER_HIT] = KnobThresholdHit.Value();
    threshold[COUNTER_MISS] = KnobThresholdMiss.Value();
    
    
    profile.SetThreshold( threshold );
    PIN_AddThreadStartFunction(ThreadStart, 0);
    PIN_AddThreadFiniFunction(ThreadFini, 0);

    TRACE_AddInstrumentFunction(Trace, 0);
    
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns

    PIN_StartProgram();
    
    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
