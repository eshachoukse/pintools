/*BEGIN_LEGAL 
  Intel Open Source License 

  Copyright (c) 2002-2015 Intel Corporation. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */
//
// @ORIGINAL_AUTHOR: Artur Klauser
//

/*! @file
 *  This file contains a configurable cache class
 */

#ifndef PIN_CACHE_H
#define PIN_CACHE_H

#define KILO 1024
#define MEGA (KILO*KILO)
#define GIGA (KILO*MEGA)
#define DRAM_SIZE 64*MEGA
#define DRAM_ASSO 16
#define ACCESS_GRAN 0 //Cache line level accesses
#define BINS 8
//#define ACCESS_GRAN 12 //Page line level accesses

typedef UINT64 CACHE_STATS; // type of cache hit/miss counters

#include "BPCompressor.hh"
#include "common.hh"
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <map>
#include <list>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <algorithm>
static string mydecstr(UINT64 v, UINT32 w)
{
	ostringstream o;
	o.width(w);
	o << v;
	string str(o.str());
	return str;
}

/*!
 *  @brief Checks if n is a power of 2.
 *  @returns true if n is power of 2
 */
static inline bool IsPower2(UINT32 n)
{
	return ((n & (n - 1)) == 0);
}

/*!
 *  @brief Computes floor(log2(n))
 *  Works by finding position of MSB set.
 *  @returns -1 if n == 0.
 */
static inline INT32 FloorLog2(UINT32 n)
{
	INT32 p = 0;

	if (n == 0) return -1;

	if (n & 0xffff0000) { p += 16; n >>= 16; }
	if (n & 0x0000ff00)	{ p +=  8; n >>=  8; }
	if (n & 0x000000f0) { p +=  4; n >>=  4; }
	if (n & 0x0000000c) { p +=  2; n >>=  2; }
	if (n & 0x00000002) { p +=  1; }

	return p;
}

static unsigned bin(UINT32 size) {
	int frag_mode;
	if(BINS == 8)
		frag_mode = 2;
	else
		frag_mode = 3;
	for (unsigned i=1; i<sizeof(block_sizes[frag_mode]); i++) {
		if(size > block_sizes[frag_mode][i]) {
			return block_sizes[frag_mode][i-1];
		}
	}
	return block_sizes[frag_mode][sizeof(block_sizes[frag_mode])-1];
}

/*!
 *  @brief Computes floor(log2(n))
 *  Works by finding position of MSB set.
 *  @returns -1 if n == 0.
 */
static inline INT32 CeilLog2(UINT32 n)
{
	return FloorLog2(n - 1) + 1;
}

/*!
 *  @brief Cache tag - self clearing on creation
 */
class CACHE_TAG
{
	private:
		ADDRINT _tag;

	public:
		CACHE_TAG(ADDRINT tag = 0) { _tag = tag; }
		bool operator==(const CACHE_TAG &right) const { return _tag == right._tag; }
		operator ADDRINT() const { return _tag; }
};



namespace CACHE_ALLOC
{
	typedef enum 
	{
		STORE_ALLOCATE,
		STORE_NO_ALLOCATE
	} STORE_ALLOCATION;
}

/*!
 *  @brief Generic cache base class; no allocate specialization, no cache set specialization
 */
class CACHE_BASE
{
	public:
		// types, constants
		typedef enum 
		{
			ACCESS_TYPE_LOAD,
			ACCESS_TYPE_STORE,
			ACCESS_TYPE_NUM
		} ACCESS_TYPE;

		typedef enum
		{
			CACHE_TYPE_ICACHE,
			CACHE_TYPE_DCACHE,
			CACHE_TYPE_NUM
		} CACHE_TYPE;

	protected:
		static const UINT32 HIT_MISS_NUM = 2;
		CACHE_STATS _access[ACCESS_TYPE_NUM][HIT_MISS_NUM];
		CACHE_STATS _size_changes_inc;
		CACHE_STATS _size_changes_dec;
		CACHE_STATS _size_same_on_wb;
		CACHE_STATS _mem_reads;
		CACHE_STATS _mem_unique_accesses;
		CACHE_STATS _mem_writes;
		CACHE_STATS _mem_unique_writes;

	private:    // input params
		const std::string _name;
		const UINT32 _cacheSize;
		const UINT32 _associativity;

		// computed params
		const UINT32 _setIndexMask;

		CACHE_STATS SumAccess(bool hit) const
		{
			CACHE_STATS sum = 0;

			for (UINT32 accessType = 0; accessType < ACCESS_TYPE_NUM; accessType++)
			{
				sum += _access[accessType][hit];
			}

			return sum;
		}

	protected:
		UINT32 NumSets() const { return _setIndexMask + 1; }

	public:
		const UINT32 _lineSize;
		const UINT32 _lineShift;
		// constructors/destructors
		CACHE_BASE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity);

		// accessors
		UINT32 CacheSize() const { return _cacheSize; }
		UINT32 LineSize() const { return _lineSize; }
		UINT32 Associativity() const { return _associativity; }
		//
		CACHE_STATS Hits(ACCESS_TYPE accessType) const { return _access[accessType][true];}
		CACHE_STATS Misses(ACCESS_TYPE accessType) const { return _access[accessType][false];}
		CACHE_STATS Accesses(ACCESS_TYPE accessType) const { return Hits(accessType) + Misses(accessType);}
		CACHE_STATS Hits() const { return SumAccess(true);}
		CACHE_STATS Misses() const { return SumAccess(false);}
		CACHE_STATS Accesses() const { return Hits() + Misses();}
		CACHE_STATS Size_changes_inc() const { return _size_changes_inc;}
		CACHE_STATS Size_changes_dec() const { return _size_changes_dec;}
		CACHE_STATS Size_sameonWB() const { return _size_same_on_wb;}
		CACHE_STATS Mem_Reads() const { return _mem_reads;}
		CACHE_STATS Mem_Writes() const { return _mem_writes;}
		CACHE_STATS Mem_UniqueAccesses() const { return _mem_unique_accesses;}
		CACHE_STATS Mem_UniqueWrites() const { return _mem_unique_writes;}

		VOID SplitAddress(const ADDRINT addr, CACHE_TAG & tag, UINT32 & setIndex) const
		{
			tag = addr >> _lineShift;
			setIndex = tag & _setIndexMask;
		}

		VOID SplitAddress(const ADDRINT addr, CACHE_TAG & tag, UINT32 & setIndex, UINT32 & lineIndex) const
		{
			const UINT32 lineMask = _lineSize - 1;
			lineIndex = addr & lineMask;
			SplitAddress(addr, tag, setIndex);
		}

		string StatsLong(string prefix = "", CACHE_TYPE = CACHE_TYPE_DCACHE, bool compress = 0) const;
};

	CACHE_BASE::CACHE_BASE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity)
: _name(name),
	_cacheSize(cacheSize),
	_lineSize(lineSize),
	_associativity(associativity),
	_lineShift(FloorLog2(lineSize)),
	_setIndexMask((cacheSize / (associativity * lineSize)) - 1)
{

	ASSERTX(IsPower2(_lineSize));
	ASSERTX(IsPower2(_setIndexMask + 1));

	for (UINT32 accessType = 0; accessType < ACCESS_TYPE_NUM; accessType++)
	{
		_access[accessType][false] = 0;
		_access[accessType][true] = 0;
	}
	_size_changes_inc = 0;
	_size_changes_dec = 0;
	_size_same_on_wb = 0;
	_mem_reads=0;
	_mem_unique_accesses=0;
	_mem_writes=0;
	_mem_unique_writes=0;
}

/*!
 *  @brief Stats output method
 */


string CACHE_BASE::StatsLong(string prefix, CACHE_TYPE cache_type, bool compress) const
{
	const UINT32 headerWidth = 19;
	const UINT32 numberWidth = 12;

	string out;

	out += prefix + _name + ":" + "\n";
	if (cache_type != CACHE_TYPE_ICACHE) {
		for (UINT32 i = 0; i < ACCESS_TYPE_NUM; i++)
		{
			const ACCESS_TYPE accessType = ACCESS_TYPE(i);

			std::string type(accessType == ACCESS_TYPE_LOAD ? "Load" : "Store");

			out += prefix + ljstr(type + "-Hits:      ", headerWidth)
				+ mydecstr(Hits(accessType), numberWidth)  +
				"  " +fltstr(100.0 * Hits(accessType) / Accesses(accessType), 2, 6) + "%\n";

			out += prefix + ljstr(type + "-Misses:    ", headerWidth)
				+ mydecstr(Misses(accessType), numberWidth) +
				"  " +fltstr(100.0 * Misses(accessType) / Accesses(accessType), 2, 6) + "%\n";

			out += prefix + ljstr(type + "-Accesses:  ", headerWidth)
				+ mydecstr(Accesses(accessType), numberWidth) +
				"  " +fltstr(100.0 * Accesses(accessType) / Accesses(accessType), 2, 6) + "%\n";

			out += prefix + "\n";
		}
	}

	out += prefix + ljstr("Total-Hits:      ", headerWidth)
		+ mydecstr(Hits(), numberWidth) +
		"  " +fltstr(100.0 * Hits() / Accesses(), 2, 6) + "%\n";

	out += prefix + ljstr("Total-Misses:    ", headerWidth)
		+ mydecstr(Misses(), numberWidth) +
		"  " +fltstr(100.0 * Misses() / Accesses(), 2, 6) + "%\n";

	if(compress) {
		out += prefix + ljstr("Total-Size Changes inc:      ", headerWidth)
			+ mydecstr(Size_changes_inc(), numberWidth) +
			"  " +fltstr(100.0 * Size_changes_inc() / Mem_Writes(), 2, 6) + "%\n";

		out += prefix + ljstr("Total-Size Changes dec:      ", headerWidth)
			+ mydecstr(Size_changes_dec(), numberWidth) +
			"  " +fltstr(100.0 * Size_changes_dec() / Mem_Writes(), 2, 6) + "%\n";

		out += prefix + ljstr("Total-Size Same on WB:      ", headerWidth)
			+ mydecstr(Size_sameonWB(), numberWidth) +
			"  " +fltstr(100.0 * Size_sameonWB() / Mem_Writes(), 2, 6) + "%\n";
	}

	out += prefix + ljstr("Total-MemReads:  ", headerWidth)
		+ mydecstr(Mem_Reads(), numberWidth) +
		"  " +fltstr(100.0 * Mem_Reads() / Accesses(), 2, 6) + "%\n";
	out += prefix + ljstr("Total-MemWrites:  ", headerWidth)
		+ mydecstr(Mem_Writes(), numberWidth) +
		"  " +fltstr(100.0 * Mem_Writes() / Accesses(), 2, 6) + "%\n";
	out += prefix + ljstr("Total-UniqueMemWrites as percentage of total mem writes:  ", headerWidth)
		+ mydecstr(Mem_UniqueWrites(), numberWidth) +
		"  " +fltstr(100.0 * Mem_UniqueWrites() / Mem_Writes(), 2, 6) + "%\n";
	out += prefix + ljstr("Total-UniqueMemAccesses:  ", headerWidth)
		+ mydecstr(Mem_UniqueAccesses(), numberWidth) +
		"  " +fltstr(100.0 * Mem_UniqueAccesses() / Accesses(), 2, 6) + "%\n";
	out += prefix + ljstr("Total-Accesses:  ", headerWidth)
		+ mydecstr(Accesses(), numberWidth) +
		"  " +fltstr(100.0 * Accesses() / Accesses(), 2, 6) + "%\n";

	out += "\n";

	return out;
}

template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
class CACHE;

/*!
 * Everything related to cache sets
 */
namespace CACHE_SET
{

	/*!
	 *  @brief Cache set direct mapped
	 */
	class DIRECT_MAPPED
	{
		private:
			CACHE_TAG _tag;
			UINT32 _lineSize;
			UINT32 _lineShift;

		public:
			DIRECT_MAPPED(UINT32 associativity = 1) { 
				ASSERTX(associativity == 1); 
			}

			VOID SetAssociativity(UINT32 associativity) { ASSERTX(associativity == 1); }
			UINT32 GetAssociativity(UINT32 associativity) { return 1; }

			VOID SetSize(UINT32 lsi, UINT32 lsh) {
				_lineSize = lsi;
				_lineShift = lsh;
			}

			UINT32 Find(CACHE_TAG tag) { return(_tag == tag); }
			UINT32 FindWrite(CACHE_TAG tag) { return(_tag == tag); }
			UINT32 Replace(CACHE_TAG tag, bool traceAddr, bool traceData) { _tag = tag; return 1;}
			UINT32 Flush(UINT32 index) { return 1;}
			UINT32 CompressedLineSize(UINT32 index) { return 1;}
	};

	/*!
	 *  @brief Cache set with round robin replacement
	 */
	template <UINT32 MAX_ASSOCIATIVITY = 4, bool cacheType = 0> //0:LLC, 1:DRAM
		class ROUND_ROBIN
		{
			private:
				UINT32 _tagsLastIndex;
				UINT32 _nextReplaceIndex;
				bool _dirty[MAX_ASSOCIATIVITY];
				CACHELINE_DATA _data[MAX_ASSOCIATIVITY];
				unsigned	_size[MAX_ASSOCIATIVITY];
				unsigned	_insize[MAX_ASSOCIATIVITY];
				unsigned	_maxSize[MAX_ASSOCIATIVITY];
				int		_stepsToMaxSize[MAX_ASSOCIATIVITY];
				int		_stepsToEvict[MAX_ASSOCIATIVITY];
				UINT32  _refs[MAX_ASSOCIATIVITY];
				UINT32  _sumRefs;
				UINT32  _sumStepsMax, _sumStepsEvict, _sumMaxSizeWB, _sumSameSizeWB;
				UINT32 	_sumCOverflows, _sumCUnderflows;
				UINT64  _accessesAtRead[MAX_ASSOCIATIVITY];
				UINT64  _sumAccessesAtRead;
				Compressor *mycomp;
				UINT32 _lineSize;
				UINT32 _lineShift;
				std::ofstream _outfile;
				UINT64  mem_writes;
				UINT64 mem_reads;
				INT64 _currsize;
				UINT32 Name;

			public:
				std::map<UINT64,int> memWrite_map;
				std::map<UINT64,std::pair<int, unsigned>> memAccess_map;
				CACHE_TAG _tags[MAX_ASSOCIATIVITY];
				ROUND_ROBIN( UINT32 associativity = MAX_ASSOCIATIVITY)
					: _tagsLastIndex(associativity - 1)
				{
					ASSERTX(associativity <= MAX_ASSOCIATIVITY);
					_nextReplaceIndex = _tagsLastIndex;
					//cout << "It is at " << getcwd(buf, 50) << "\n";
					//_outfile.open("~/Documents/Snap-2.4/examples/centrality/PinTraceFile.out");
					//mycomp = new BPSCompressor64("BPC64_5", 2, 4, 10, 3);   // ESHA GOLDEN
					mycomp = new BPSCompressor64("BPC64_5", 2, 4, 10, 2);   // ESHA GOLDEN // 64B, 8bins

					for (INT32 index = _tagsLastIndex; index >= 0; index--)
					{
						_tags[index] = CACHE_TAG(0);
						_dirty[index]=0;
						_size[index] = 0;
						_insize[index] = 0;
						_refs[index]=0;
						_accessesAtRead[index]=0;
						_maxSize[index]=0;
					}
					mem_writes=0;
					mem_reads=0;
					_currsize=0;
					_sumRefs=0;
					_sumStepsMax=0;
					_sumStepsEvict=0;
					_sumMaxSizeWB=0;
					_sumSameSizeWB=0;
					_sumCOverflows=0;
					_sumCUnderflows=0;
					_sumAccessesAtRead=0;
					Name = 0;
				}

				~ROUND_ROBIN(){
					//_outfile.close();
				} 

				VOID SetSize(UINT32 lsi, UINT32 lsh) {
					_lineSize = lsi;
					_lineShift = lsh;
				}

				VOID SetName(UINT32 name) { Name = name; }
				VOID SetAssociativity(UINT32 associativity)
				{
					ASSERTX(associativity <= MAX_ASSOCIATIVITY);
					_tagsLastIndex = associativity - 1;
					_nextReplaceIndex = _tagsLastIndex;
				}
				UINT32 GetAssociativity(UINT32 associativity) { return _tagsLastIndex + 1; }

				UINT32 Find(CACHE_TAG tag)
				{
					bool result = true;

					for (INT32 index = _tagsLastIndex; index >= 0; index--)
					{
						// this is an ugly micro-optimization, but it does cause a
						// tighter assembly loop for ARM that way ...
						if(_tags[index] == tag) {
							goto end;
						}
					}
					result = false;

					end: return result;
				}

				UINT32 FindWrite(CACHE_TAG tag)
				{
					bool result = true;
					uint64_t line_addr;

					for (INT32 index = _tagsLastIndex; index >= 0; index--)
					{
						// this is an ugly micro-optimization, but it does cause a
						// tighter assembly loop for ARM that way ...
						if(_tags[index] == tag){
							_dirty[index] = 1;
							_refs[index]++;
							//Uncomment below if you want to check compressibility at a given time
							ADDRINT addr = tag << _lineShift;
							PIN_SafeCopy(&_data[index], (VOID *) addr, _lineSize);
							unsigned newSize =  bin(mycomp->compressLine(&_data[index], line_addr));
							if(newSize > _maxSize[index]){
								_maxSize[index] = newSize;
								_stepsToMaxSize[index]++;
							}
							if(newSize != _size[index])
								_stepsToEvict[index]++;
							_size[index] = newSize;
							//printf("Line made dirty \n");
							goto end;
						}
					}
					result = false;

					end: return result;
				}

				UINT32 Replace(CACHE_TAG tag,  bool traceAddr, bool traceData, bool compress, std::ofstream& _outfile, ADDRINT * oldaddr, UINT64 curr_accesses)
				{
					// g++ -O3 too dumb to do CSE on following lines?!
					const UINT32 index = _nextReplaceIndex;
					uint64_t line_addr;
					UINT32 rval=0;
					bool unique=0;

					//cout << "In accersss : " << traceAddr << "\n";
					ADDRINT newaddr = tag << _lineShift;
					*oldaddr = _tags[index] << _lineShift;
					ADDRINT oldaddrval = *oldaddr;
					_tags[index] = tag;
					// condition typically faster than modulo
					_nextReplaceIndex = (index == 0 ? _tagsLastIndex : index - 1);
					mem_reads++;
					if(_dirty[index]) {
						mem_writes++;

						if(memWrite_map.find(*oldaddr)!=memWrite_map.end())
							memWrite_map[*oldaddr]++;
						else
							memWrite_map[*oldaddr]=1;

						if(memAccess_map.find(*oldaddr >> ACCESS_GRAN)!=memAccess_map.end())
							memAccess_map[*oldaddr >> ACCESS_GRAN].first++;
						else
							memAccess_map[*oldaddr >> ACCESS_GRAN].first=1;

						if(compress || traceData)
							PIN_SafeCopy(&_data[index], (VOID *) oldaddrval, _lineSize);
						if(compress) {	
							unsigned WB_size = bin(mycomp->compressLine(&_data[index], line_addr));
							//printf("Writing back dirty line old size: %d new: %d \n", _size[index], WB_size);
							memAccess_map[*oldaddr >> ACCESS_GRAN].second = WB_size;
							if(WB_size > _size[index]) {
								rval= 1;
								_currsize+=(WB_size - _size[index]);
							}
							else if(WB_size < _size[index]) {
								rval=2;
								_currsize-=(_size[index] - WB_size);
							}
							else if(WB_size == _size[index])
								rval=3;
							//_outfile << "WB_" << Name << " " << " " << hex << *oldaddr << dec << " " << WB_size << " " << _size[index] << " " << _currsize << " " << memAccess_map.size()*512 << "\n"; 
							//if(_currsize >  memAccess_map.size()*512)
							//	 _outfile << "woah! \n";
						}
						else
							rval = 1;
						if(traceData) {
							_outfile << "W "<< hex << *oldaddr << " " ;
							for(int i=0; i<_MAX_QWORDS_PER_LINE; i++)
								_outfile << _data[index].qword[i] << " ";
							_outfile << dec << " \n"; 
						}
						else if (traceAddr) {
							_outfile << "W "<< hex << *oldaddr << dec << " \n"; 
						}
						_sumRefs+=_refs[index];
						_sumStepsMax += _stepsToMaxSize[index];
						_sumStepsEvict += _stepsToEvict[index];
						if(_stepsToEvict[index] && (_maxSize[index]==_size[index]))
							_sumMaxSizeWB++;
						else if (_stepsToEvict[index]==0)
							_sumSameSizeWB++;
						if(_insize[index] < _size[index])
							_sumCOverflows++;
						else if(_insize[index] > _size[index])
							_sumCUnderflows++;
					}
					if(curr_accesses > _accessesAtRead[index])
						_sumAccessesAtRead+=(curr_accesses-_accessesAtRead[index]);
					_refs[index]=0;
					_accessesAtRead[index]=curr_accesses;
					if(memAccess_map.find(newaddr >> ACCESS_GRAN)!=memAccess_map.end())
						memAccess_map[newaddr >> ACCESS_GRAN].first++;
					else {
						memAccess_map[newaddr >> ACCESS_GRAN].first=1;
						unique = 1;
					}

					if(compress || traceData)
						PIN_SafeCopy(&_data[index], (VOID *) newaddr, _lineSize);
					if(compress) {
						_size[index] = bin(mycomp->compressLine(&_data[index], line_addr));
						_insize[index]=_size[index];
						_maxSize[index] = _size[index];
						_stepsToMaxSize[index]=0;
						_stepsToEvict[index]=0;
						if(unique) {
							memAccess_map[newaddr >> ACCESS_GRAN].second = _size[index];
							_currsize+=_size[index];
							//_outfile << "R_" << Name << " " << " " << hex << newaddr << dec << " " << _size[index] << " " << _currsize << " " << memAccess_map.size() * 512<< "\n";
						}
						else {
							if(memAccess_map[newaddr >> ACCESS_GRAN].second != _size[index]) {
								//_outfile << "Rwhy_" << Name << " " << hex << newaddr << dec << " " << _size[index] << " " << _currsize << " " << memAccess_map.size() * 512<< "\n";
								if(memAccess_map[newaddr >> ACCESS_GRAN].second > _size[index])
									_currsize-=(memAccess_map[newaddr >> ACCESS_GRAN].second - _size[index]);
								else
									_currsize+=(_size[index] - memAccess_map[newaddr >> ACCESS_GRAN].second);
								//_outfile << "afterRwhy_" << Name << " " << hex << newaddr << dec << " " << _size[index] << " " << _currsize << " " << memAccess_map.size() * 512<< "\n";
								memAccess_map[newaddr >> ACCESS_GRAN].second = _size[index];
							}
						}
					}
					if(traceData) {
						_outfile << "R "<< hex << newaddr << " " ;
						for(int i=0; i<_MAX_QWORDS_PER_LINE; i++)
							_outfile << _data[index].qword[i] << " ";
						_outfile << dec << " \n"; 
					}
					else if(traceAddr)
						_outfile << "R "<< hex << newaddr << dec << " \n"; 
					//printf("Data is compressed to %d bits \n", _size[index]);
					_dirty[index] = 0;
					return rval;
				}

				UINT32 Flush(UINT32 index, bool traceAddr, bool traceData, bool compress)
				{
					UINT32 rval = 0;
					uint64_t line_addr;
					if(_dirty[index]) {
						mem_writes++;
						ADDRINT oldaddr = _tags[index] << _lineShift;
						if(memWrite_map.find(oldaddr)!=memWrite_map.end())
							memWrite_map[oldaddr]++;
						else
							memWrite_map[oldaddr]=1;

						if(memAccess_map.find(oldaddr >> ACCESS_GRAN)!=memAccess_map.end())
							memAccess_map[oldaddr >> ACCESS_GRAN].first++;
						else
							memAccess_map[oldaddr >> ACCESS_GRAN].first=1;

						if(compress || traceData)
							PIN_SafeCopy(&_data[index], (VOID *) oldaddr, _lineSize);
						if(compress) {	
							UINT32 WB_size = mycomp->compressLine(&_data[index], line_addr);
							//printf("Writing back dirty line old size: %d new: %d \n", _size[index], WB_size);
							//_outfile << "WB " << WB_size << " " <<  _size[index] << "\n";
							if(WB_size > _size[index]) {
								rval= 1;
								_currsize+=(WB_size - _size[index]);
							}
							else if(WB_size < _size[index]) {
								rval=2;
								_currsize-=(_size[index] - WB_size);
							}
							else if(WB_size == _size[index])
								rval=3;
						}
						else
							rval = 1;
						if(traceData) {
							_outfile << "W "<< hex << oldaddr << " " ;
							for(int i=0; i<_MAX_QWORDS_PER_LINE; i++)
								_outfile << _data[index].qword[i] << " ";
							_outfile << dec << " \n"; 
						}
						else if (traceAddr) {
							_outfile << "W "<< hex << oldaddr << dec << " \n"; 
						}
					}
					return rval;
				}

				UINT64 getmem_reads() { return mem_reads;}
				UINT64 getmem_writes() { return mem_writes;}
				UINT64 getmem_uniquewrites() { return memWrite_map.size();}
				UINT64 getmem_uniqueaccesses() { return memAccess_map.size();}
				UINT64 getmem_currsize() {return _currsize;}
				UINT64 getsum_accesses() {return _sumAccessesAtRead;}
				UINT32 getsum_refs() {return _sumRefs;}
				UINT32 getsum_StepsMax() {return _sumStepsMax;}
				UINT32 getsum_StepsEvict() {return _sumStepsEvict;}
				UINT32 getsum_MaxSizeWB() {return _sumMaxSizeWB;}
				UINT32 getsum_SameSizeWB() {return _sumSameSizeWB;}
				UINT32 getsum_COverflows() {return _sumCOverflows;}
				UINT32 getsum_CUnderflows() {return _sumCUnderflows;}
				void resetRefs() { 
					_sumRefs=0; 
					_sumAccessesAtRead=0;
					_sumStepsMax=0;
					_sumStepsEvict=0;
					_sumMaxSizeWB=0;
					_sumSameSizeWB=0;
					_sumCOverflows=0;
					_sumCUnderflows=0;
					_sumAccessesAtRead=0;
				}
				UINT32 CompressedLineSize(UINT32 index)
				{
					uint64_t line_addr;
					if(_dirty[index])
						return mycomp->compressLine(&_data[index], line_addr);
					else
						return _size[index];

				} 
		};

} // namespace CACHE_SET

/*!
 *  @brief Templated cache class with specific cache set allocation policies
 *
 *  All that remains to be done here is allocate and deallocate the right
 *  type of cache sets.
 */
template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
class CACHE : public CACHE_BASE
{
	private:
		SET _sets[MAX_SETS];
		UINT32 _compressedSizes[1000][MAX_SETS*4];
		UINT32 curr_loop;
		CACHE<CACHE_SET::ROUND_ROBIN<DRAM_ASSO>, DRAM_SIZE/(64*DRAM_ASSO), STORE_ALLOCATION> *myDram;
		UINT32 prev_mem_writes;
		UINT32 eviction;

	public:
		// constructors/destructors
		CACHE(std::string name, UINT32 cacheSize, UINT32 lineSize, UINT32 associativity, UINT32 DRAMtype, std::ostream& outfile)
			: CACHE_BASE(name, cacheSize, lineSize, associativity)
		{
			cout << "NumSets() " << NumSets() << " Associativity " << associativity << " DRAM cache type " << DRAMtype <<std::endl;
			ASSERTX(NumSets() <= MAX_SETS);
			curr_loop = 0;
			prev_mem_writes=0;
			eviction=0;

			for (UINT32 i = 0; i < NumSets(); i++)
			{
				_sets[i].SetAssociativity(associativity);
				_sets[i].SetSize(_lineSize, _lineShift);
				_sets[i].SetName(i);
			}
			if(DRAMtype){
    			myDram = new CACHE<CACHE_SET::ROUND_ROBIN<DRAM_ASSO>, DRAM_SIZE/(64*DRAM_ASSO) , STORE_ALLOCATION>("DRAM cache", 
                         DRAM_SIZE,
                         64,
			 DRAM_ASSO,
                         0, outfile);
			}
			else {
				myDram = NULL;
			}

		}

		// modifiers
		/// Cache access from addr to addr+size-1
		bool Access(ADDRINT addr, UINT32 size, ACCESS_TYPE accessType, bool traceAddr, bool traceData, bool compress, int DRAMCacheType , std::ofstream& outfile);
		/// Cache access at addr that does not span cache lines
		bool AccessSingleLine(ADDRINT addr, ACCESS_TYPE accessType, bool traceAddr, bool traceData, bool compress, int DRAMCacheType , std::ofstream& outfile);

		void FlushAll(bool traceAddr, bool traceData, bool compress, int DRAMCacheType, std::ofstream&);

		void PrintMemUnique(std::ofstream& outfile);

		void PrintMemAccessUnique(std::ofstream& outfile);

		string PrintDRAMStats();

		string PrintCompression();

		string PrintCurrWrites();

		string PrintCurrCompressibility();

		string PrintCurrAccessesBeforeEviction();
		
		string PrintCurrStepsToMaxComp();
		
		string PrintCurrStepsToEviction();
		
		string PrintCurrWasWBMax();

		string PrintCurrCOverflows();

		void ResetRefs();
};

/*!
 *  @return true if all accessed cache lines hit
 */

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
bool CACHE<SET,MAX_SETS,STORE_ALLOCATION>::Access(ADDRINT addr, UINT32 size, ACCESS_TYPE accessType, bool traceAddr, bool traceData, bool compress, int DRAMCacheType , std::ofstream& outfile)
{
	const ADDRINT highAddr = addr + size;
	bool allHit = true;

	const ADDRINT lineSize = LineSize();
	const ADDRINT notLineMask = ~(lineSize - 1);
	do
	{
		CACHE_TAG tag;
		UINT32 setIndex;

		SplitAddress(addr, tag, setIndex);

		SET & set = _sets[setIndex];

		/*
		   if(Accesses()%100000==0) {
		   if(curr_loop<1000) {
		   for(int i=0; i<NumSets(); i++) {
		   for(int j=0; j<4 ;j++)
		   _compressedSizes[curr_loop][i*4+j] = _sets[i].CompressedLineSize(j);
		   }
		   curr_loop++;
		   }
		   }
		 */

		bool localHit ;
		if(accessType == ACCESS_TYPE_LOAD)
			localHit= set.Find(tag);
		else
			localHit= set.FindWrite(tag);
		allHit &= localHit;

		// on miss, loads always allocate, stores optionally
		if ( (! localHit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
		{
			ADDRINT oldAddr;
			UINT32 rval = set.Replace(tag, traceAddr, traceData, compress, outfile, &oldAddr, Accesses());
			if(rval > 0)
				eviction++;
			if((rval>0) && (DRAMCacheType>0)) {
				myDram->AccessSingleLine(oldAddr, CACHE_BASE::ACCESS_TYPE_STORE, 0, 0, 0, 0, outfile);    
			}
			if(DRAMCacheType==1) {
				myDram->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD, 0, 0, 0, 0, outfile);    
			}

			if(rval==1)
				_size_changes_inc++;
			else if(rval==2)
				_size_changes_dec++;
			else if(rval==3)
				_size_same_on_wb++;
		}

		addr = (addr & notLineMask) + lineSize; // start of next cache line
	}
	while (addr < highAddr);

	_access[accessType][allHit]++;

	return allHit;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCompression()
{
	string out;
	const UINT32 numberWidth = 6;
	for (int i=0; i<1000; i++)
	{
		out+="\n";
		for(int j=0; j<(NumSets()*4); j++)
			out+=mydecstr(_compressedSizes[i][j], numberWidth) + " ";
	}
	return out;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintDRAMStats()
{
	return myDram->StatsLong("# ", CACHE_BASE::CACHE_TYPE_DCACHE, 0);	
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrWrites()
{
	string out;
	const UINT32 numberWidth = 6;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		_mem_writes+=_sets[i].getmem_writes();
		_mem_reads+=_sets[i].getmem_reads();
	}
	out+=mydecstr(_mem_writes, numberWidth) + " " + mydecstr(_mem_reads, numberWidth) + "\n";
	prev_mem_writes=_mem_writes;
	_mem_writes=0;
	_mem_reads=0;
	return out;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrStepsToMaxComp()
{
	string out;
	const UINT32 numberWidth = 6;
	UINT64 totalStepstoMax = 0;
	UINT64 totalStepstoEvict = 0;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		totalStepstoMax+=_sets[i].getsum_StepsMax();
		totalStepstoEvict+=_sets[i].getsum_StepsEvict();
	}
	if(eviction)
		out+=fltstr((float)totalStepstoMax/(float)eviction, 2, numberWidth) + " " + fltstr((float)totalStepstoEvict/(float)eviction, 2, numberWidth)+"\n";
	eviction=0;
	return out;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrStepsToEviction()
{
	string out;
	const UINT32 numberWidth = 6;
	UINT64 totalStepstoEvict = 0;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		totalStepstoEvict+=_sets[i].getsum_StepsEvict();
	}
	if(eviction)
		out+=fltstr((float)totalStepstoEvict/(float)eviction, numberWidth) + "\n";
	eviction=0;
	return out;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrWasWBMax()
{
	string out;
	const UINT32 numberWidth = 6;
	UINT64 totalWasWBMax = 0;
	UINT64 totalWasSame = 0;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		totalWasWBMax+=_sets[i].getsum_MaxSizeWB();
		totalWasSame+=_sets[i].getsum_SameSizeWB();
	}
	if(eviction)
		out+=fltstr((float)totalWasWBMax/(float)eviction, 2, numberWidth) + " " + fltstr((float)totalWasSame/(float)eviction, 2, numberWidth) +"\n";
	eviction=0;
	return out;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrCOverflows()
{
	string out;
	const UINT32 numberWidth = 6;
	UINT64 totalCOver = 0;
	UINT64 totalCUnder = 0;
	UINT64 totalCSame = 0;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		totalCOver+=_sets[i].getsum_COverflows();
		totalCUnder+=_sets[i].getsum_CUnderflows();
		totalCSame+=_sets[i].getsum_SameSizeWB();
	}
	if(eviction)
		out+=fltstr((float)totalCOver/(float)eviction, 2, numberWidth) + " " + fltstr((float)totalCUnder/(float)eviction, 2, numberWidth) + " " + fltstr((float)totalCSame/(float)eviction, 2, numberWidth) +"\n";
	eviction=0;
	return out;
}

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrAccessesBeforeEviction()
{
	string out;
	const UINT32 numberWidth = 6;
	UINT64 totalEvictDist = 0;
	UINT32 totalRefs = 0;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		totalEvictDist+=_sets[i].getsum_accesses();
		totalRefs+=_sets[i].getsum_refs();
	}
	if(eviction)
		out+=mydecstr(totalEvictDist/eviction, numberWidth) + " " + mydecstr(totalRefs/eviction, numberWidth) + "\n";
	eviction=0;
	return out;
}
	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
void CACHE<SET,MAX_SETS,STORE_ALLOCATION>::ResetRefs()
{
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		_sets[i].resetRefs();
	}
}
	
//Compressibility wrt addresses touched so far
	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
string CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintCurrCompressibility()
{
	string out;
	const UINT32 numberWidth = 6;
	UINT64 curr_size =0;	
	UINT64 full_size;
	float compression;
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		_mem_unique_accesses+=_sets[i].getmem_uniqueaccesses();
		curr_size+=_sets[i].getmem_currsize();
	}
	full_size = _mem_unique_accesses*64*8;
	if(curr_size) {
		compression = (float)full_size/(float)curr_size;
		out+=fltstr(compression, 2, 6) + "\n";
	}
	_mem_unique_accesses=0;
	return out;
}
	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
void CACHE<SET,MAX_SETS,STORE_ALLOCATION>::FlushAll(bool traceAddr, bool traceData, bool compress, int DRAMCacheType, std::ofstream& outfile)
{
	for (UINT32 i = 0; i < NumSets(); i++)
	{
		for (UINT32 j=0; j<4; j++) {
			UINT32 rval = _sets[i].Flush(j, traceAddr, traceData, compress);
			if((rval>0) && (DRAMCacheType>0)) {
				myDram->AccessSingleLine(_sets[i]._tags[j] << _lineShift, CACHE_BASE::ACCESS_TYPE_STORE, 0, 0, 0, 0, outfile);    
			}
			if(rval==1)
				_size_changes_inc++;
			else if(rval==2)
				_size_changes_dec++;
			else if(rval==3)
				_size_same_on_wb++;
		}
		_mem_reads+=_sets[i].getmem_reads();
		_mem_writes+=_sets[i].getmem_writes();
		_mem_unique_writes+=_sets[i].getmem_uniquewrites();
		_mem_unique_accesses+=_sets[i].getmem_uniqueaccesses();
	}
}


class sort_map
{
	public:
		UINT64 key;
		int val;
};

	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
void CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintMemAccessUnique(std::ofstream& outfile)
{

	std::vector< sort_map > v;
	sort_map sm;
	UINT64 sum=0;
	for (UINT32 i = 0; i < NumSets(); i++)
		for (auto it = _sets[i].memAccess_map.begin(); it != _sets[i].memAccess_map.end(); ++it)
		{
			sm.key = (*it).first; sm.val = (*it).second.first;
			v.push_back(sm);
		}

	std::sort(v.begin(),v.end(), [](const sort_map& a ,const sort_map& b){ return a.val > b.val; });
	for (auto itv = v.begin(); itv != v.end(); ++itv)
	{
		//outfile << hex << (*itv).key << dec << " : " << (*itv).val << endl;
		//Cumulative
		sum+=(*itv).val;
		outfile<<sum<< endl;
	}

}
	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
void CACHE<SET,MAX_SETS,STORE_ALLOCATION>::PrintMemUnique(std::ofstream& outfile)
{

	std::vector< sort_map > v;
	sort_map sm;
	UINT64 sum=0;
	for (UINT32 i = 0; i < NumSets(); i++)
		for (auto it = _sets[i].memWrite_map.begin(); it != _sets[i].memWrite_map.end(); ++it)
		{
			sm.key = (*it).first; sm.val = (*it).second;
			v.push_back(sm);
		}

	std::sort(v.begin(),v.end(), [](const sort_map& a ,const sort_map& b){ return a.val > b.val; });
	for (auto itv = v.begin(); itv != v.end(); ++itv)
	{
		//outfile << hex << (*itv).key << dec << " : " << (*itv).val << endl;
		sum+=(*itv).val;
		outfile<<sum<< endl;
	}

}
/*!
 *  @return true if accessed cache line hits
 */
	template <class SET, UINT32 MAX_SETS, UINT32 STORE_ALLOCATION>
bool CACHE<SET,MAX_SETS,STORE_ALLOCATION>::AccessSingleLine(ADDRINT addr, ACCESS_TYPE accessType, bool traceAddr, bool traceData, bool compress, int DRAMCacheType , std::ofstream& outfile)
{
	CACHE_TAG tag;
	UINT32 setIndex;

	SplitAddress(addr, tag, setIndex);

	SET & set = _sets[setIndex];

	bool hit;
	if(accessType == ACCESS_TYPE_LOAD)
		hit = set.Find(tag);
	else
		hit = set.FindWrite(tag);

	// on miss, loads always allocate, stores optionally
	if ( (! hit) && (accessType == ACCESS_TYPE_LOAD || STORE_ALLOCATION == CACHE_ALLOC::STORE_ALLOCATE))
	{
		ADDRINT oldAddr;
		eviction++;
		UINT32 rval = set.Replace(tag, traceAddr, traceData, compress, outfile, &oldAddr, Accesses());
		if((rval>0) && (DRAMCacheType>0)) {
			myDram->AccessSingleLine(oldAddr, CACHE_BASE::ACCESS_TYPE_STORE, 0, 0, 0, 0, outfile);    
		}
		if(DRAMCacheType==1) {
			myDram->AccessSingleLine(addr, CACHE_BASE::ACCESS_TYPE_LOAD, 0, 0, 0, 0, outfile);    
		}
		if(rval==1)
			_size_changes_inc++;
		else if(rval==2)
			_size_changes_dec++;
		else if(rval==3)
			_size_same_on_wb++;
	}

	_access[accessType][hit]++;

	return hit;
}




// define shortcuts
#define CACHE_DIRECT_MAPPED(MAX_SETS, ALLOCATION) CACHE<CACHE_SET::DIRECT_MAPPED, MAX_SETS, ALLOCATION>
#define CACHE_ROUND_ROBIN(MAX_SETS, MAX_ASSOCIATIVITY, ALLOCATION) CACHE<CACHE_SET::ROUND_ROBIN<MAX_ASSOCIATIVITY>, MAX_SETS, ALLOCATION>

#endif // PIN_CACHE_H
